package com.cgapi;

import java.net.InetAddress;

public class FindMyIP {
	
    public FindMyIP() throws Exception {
        InetAddress inetAddress = InetAddress.getLocalHost();
        System.out.println("IP Address:- " + inetAddress.getHostAddress());
        System.out.println("Host Name:- " + inetAddress.getHostName());
    }
}
